let isMoving = false;
let sensorDist = 0;
let init = false;
let backoffSteps = 0;
let attackDist = 0;
let step = 0;
sensors.color3.onColorDetected(ColorSensorColor.Red, function () { })




brick.showImage(images.eyesSleeping)
control.waitMicros(100000)
brick.showImage(images.eyesPinchMiddle)
control.waitMicros(100000)
brick.showImage(images.eyesSleeping)
control.waitMicros(100000)
brick.showImage(images.eyesPinchMiddle)
control.waitMicros(100000)
brick.showImage(images.eyesPinchLeft)
control.waitMicros(100000)
brick.showImage(images.eyesPinchRight)
control.waitMicros(100000)
brick.showImage(images.eyesPinchRight)
control.waitMicros(100000)
brick.showImage(images.eyesPinchMiddle)
control.waitMicros(100000)
music.playSoundEffect(sounds.mechanicalBlip3)
brick.showImage(images.eyesAngry)




function backoff() {
    step = 100000;
    backoffSteps += 0 - 1;
    motors.largeAD.setInverted(true);
    motors.largeAD.run(100);
    control.waitMicros(step);
    if (sensors.color3.rgbRaw()[0] > 80) {
        backoffSteps = 0;
        console.log("o boi, nyt oli lähel");
        motors.largeAD.setInverted(false);
        motors.largeAD.run(100);
        control.waitMicros(2 * step);
    }
}

console.log("Waiting for initialisation");
attackDist = 50;
sensorDist = 1000;
isMoving = true;
forever(function () {
    if (brick.buttonDown.isPressed()) {
        init = true;
        for (let i = 0; i < 5; i++) {
            control.waitMicros(200000)
            console.log("ARMED")
            console.sendToScreen()
        }
    }
    if (init) {
        sensorDist = sensors.ultrasonic1.distance()
        if (backoffSteps <= 0) {
            motors.largeA.stop()
            motors.largeD.stop()
            motors.largeAD.setInverted(false)
            if (sensors.color3.rgbRaw()[0] > 80) {
                music.playSoundEffect(sounds.mechanicalBackingAlert)
                backoffSteps = 10
                console.log("nyt pakki päälle")
            }
            if (sensorDist < attackDist) {
                music.playSoundEffect(sounds.mechanicalMotorIdle)
                motors.largeAD.run(100)
                isMoving = true
                console.log("running")
            }
            if (isMoving == true && sensorDist > attackDist) {
                console.log("STOP")
                isMoving = false
                music.stopAllSounds()
                motors.stopAll()
            }
            // if nothing is going on
            if (isMoving == false && sensorDist > attackDist) {
                motors.largeA.run(60)
                motors.largeD.run(40)
            }
        } else {
            backoff()
        }
    }
    // console.logValue("R", sensors.color3.rgbRaw()[0])
    console.sendToScreen()
})