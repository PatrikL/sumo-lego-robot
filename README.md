# Sumo Lego Robot

## How to compile
 * go to [microsoft makecode for mindstorms](https://makecode.mindstorms.com/) and make a project.
 
 * paste content in main.js into javascript view
 
 * export content and move .uf2 file onto the ev3 brick using file manager

## How to run

 * Simply move a build from the builds folder onto your ev3 brick
